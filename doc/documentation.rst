Documentation
=============

.. toctree::
   :maxdepth: 2

   user_guide
   python_interface

* :ref:`genindex`
