'''
Created on May 6, 2019

@author: baumannt
'''
import unittest
import os
import numpy as np

import drever
from drever.video_filter import VideoFilter
from drever.data_handlers.image import ImageData


class Test(unittest.TestCase):

    tmp_dir = ""

    @classmethod
    def setUpClass(cls):

        # Create temporary directory for testdata files if not jet available
        cls.tmp_dir = os.path.join(drever.__path__[0], "../../tmp")
        if not os.path.exists(cls.tmp_dir):  # pragma: no cover
            os.makedirs(cls.tmp_dir)
            print("tmp directory created: " + cls.tmp_dir)

    def test_default_video_filter(self):

        set_of_valid_params = [
            {"width": 100, "height": 100, "bitdepth":  8, "channels": 1},
            {"width": 100, "height": 100, "bitdepth": 10, "channels": 2},
            {"width": 100, "height": 100, "bitdepth": 12, "channels": 3}
        ]

        for params in set_of_valid_params:
            with self.subTest(params=params):
                uut = VideoFilter()

                stimulus = ImageData(params)
                stimulus.init_with_random()

                uut.set_input_data(stimulus)
                uut.run()

                self.assertEqual(
                    uut.get_output_data(),
                    stimulus,
                    "Data missmatch!"
                )

    def test_vector_dump(self):

        ppm_files = [
            {
                "params": {
                    "width":    20,
                    "height":   10,
                    "bitdepth":  8,
                    "channels":  1
                },
                "path": "vf_8bit_gray.ppm"
            },
            {
                "params": {
                    "width":    20,
                    "height":   10,
                    "bitdepth": 10,
                    "channels":  2
                },
                "path": "vf_10bit_2chans.ppm"
            },
            {
                "params": {
                    "width":    20,
                    "height":   10,
                    "bitdepth": 16,
                    "channels":  3
                },
                "path": "vf_16bit_rgb.ppm"
            }
        ]

        uut = VideoFilter()

        for ppm_file in ppm_files:
            with self.subTest(params=ppm_file["params"]):
                uut_a = ImageData()
                uut_a.set_params(ppm_file["params"])
                uut_a.init_with_random(0)
                filename = os.path.join(self.tmp_dir, ppm_file["path"])

                # Cleanup if files exists from previous runs
                if os.path.exists(filename):  # pragma: no cover
                    os.remove(filename)

                uut.set_input_data(uut_a)
                uut.dump_input_vectors(filename)

                # Saving again must raise an error
                with self.assertRaises(FileExistsError):
                    uut.dump_input_vectors(filename)

                uut_b = ImageData()

                # Loading non existing file must raise an error
                with self.assertRaises(FileNotFoundError):
                    uut_b.load(filename + ".tmp")

                # Add empty lines to see if bypassing is working
                with open(filename, 'r') as uut_a_file:
                    origin_content = uut_a_file.read()
                    uut_a_file.close()
                with open(filename, 'w') as uut_a_file:
                    uut_a_file.write("    \n")
                    uut_a_file.write(origin_content)
                    uut_a_file.close()

                # When using two channels, they have to be set explicitly
                # after loading!
                if ppm_file["params"]["channels"] == 2:
                    uut_b.load(filename, True)
                else:
                    uut_b.load(filename)

                np.testing.assert_array_equal(
                    uut_b.get_image_data(),
                    uut_a.get_image_data(),
                    "Data Missmatch"
                )


if __name__ == "__main__":  # pragma: no cover
    unittest.main()
