'''
Created on Apr 4, 2019

@author: baumannt
'''
import unittest

from drever import pkg_info


class Test(unittest.TestCase):

    ''' Unit tests for pkg_info module '''

    def test_version(self):

        ''' Tests getter of version number '''

        version_str = str(pkg_info.MAJOR_VERSION) + "."
        version_str += str(pkg_info.MINOR_VERSION) + "."
        version_str += str(pkg_info.PATCH_VERSION)

        self.assertEqual(
            pkg_info.get_version(),
            version_str,
            "Version Number mismatch"
        )

        pkg_info.print_version()


if __name__ == "__main__":  # pragma: no cover
    unittest.main()
