import os
import math
import imageio
import numpy as np

from drever.video_filter import VideoFilter
from drever.data_handlers.image import ImageData

test_configs = [
    {
        "params": {
            "width":    11,
            "height":   21,
            "bitdepth":  8,
            "channels":  1
            },
        "dump_file": "8bit_gray.ppm"
    },
    {
        "params": {
            "width":    12,
            "height":   23,
            "bitdepth": 10,
            "channels":  1
            },
        "dump_file": "10bit_gray.ppm"
    },
    {
        "params": {
            "width":    13,
            "height":   23,
            "bitdepth": 12,
            "channels":  3
            },
        "dump_file": "12bit_gray.ppm"
    },
    {
        "params": {
            "width":    14,
            "height":   24,
            "bitdepth": 16,
            "channels":  3
            },
        "dump_file": "16bit_gray.ppm"
    }
]


class ImageIOConfig():

    def __init__(self, test_config):
        self.test_config = test_config

    def pre_config(self, output_path):
        size = (
            self.test_config["params"]["height"],
            self.test_config["params"]["width"],
            self.test_config["params"]["channels"],
        )
        max_value = math.pow(self.test_config["params"]["bitdepth"], 2) - 1
        image_data = np.random.randint(max_value, size=size, dtype=np.uint16)
        stimulus_filename = os.path.join(
            output_path,
            "input_imageio_" + self.test_config["dump_file"]
        )
        processed_filename = os.path.join(
            output_path,
            "output_imageio_" + self.test_config["dump_file"]
        )
        imageio.imwrite(stimulus_filename, image_data, "PPM-FI", flags=1)
        imageio.imwrite(processed_filename, image_data, "PPM-FI", flags=1)
        return True

    def get_generics(self):
        return dict(
            IN_FILENAME="input_imageio_" + self.test_config["dump_file"],
            OUT_FILENAME="output_imageio_" + self.test_config["dump_file"]
        )


def prepare_test_configs(tb_image_data_pkg):

    for use_bitdepth in (0, 8, 10, 12, 16, ):
        for init_type in ("random", "zero"):
            for test_config in test_configs:

                config_name = \
                    "bitdepth=%u, init_type=%s, channels=%s, dumpfile=%s" % \
                    (
                        use_bitdepth,
                        init_type,
                        test_config["params"]["channels"],
                        test_config["dump_file"]
                    )

                input_data = ImageData(test_config["params"])

                if init_type == "random":
                    input_data.init_with_random(0)
                else:
                    input_data.init_with_data(
                        np.zeros(input_data.get_np_shape(), dtype=np.uint16),
                        False,
                        test_config["params"]["bitdepth"]
                    )

                # The default video filter is a simple bypass.
                uut = VideoFilter(
                    input_data=input_data,
                    dump_filename=test_config["dump_file"]
                )

                generics = uut.vunit_generate_generics()

                if use_bitdepth == 0:
                    exp_bitdepth = test_config["params"]["bitdepth"]
                else:
                    exp_bitdepth = use_bitdepth

                generics.update(
                        USE_BITDEPTH=use_bitdepth,
                        EXP_BITDEPTH=exp_bitdepth,
                        CHANNELS=test_config["params"]["channels"]
                )

                tb_image_data_pkg.set_sim_option(
                    "vhdl_assert_stop_level", "failure")

                tb_image_data_pkg.add_config(
                    name="drever:" + config_name,
                    pre_config=uut.vunit_pre_config,
                    generics=generics
                )

                imageio_config = ImageIOConfig(test_config)
                generics = imageio_config.get_generics()

                if use_bitdepth == 0:
                    exp_bitdepth = 16  # ImageIO has always max. value of 65535
                else:
                    exp_bitdepth = use_bitdepth

                generics.update(
                        USE_BITDEPTH=use_bitdepth,
                        EXP_BITDEPTH=exp_bitdepth,
                        CHANNELS=test_config["params"]["channels"]
                )

                tb_image_data_pkg.add_config(
                    name="imageio:" + config_name,
                    pre_config=imageio_config.pre_config,
                    generics=generics
                )
