import numpy as np

from drever.stream_filter import StreamFilter
from drever.data_handlers.stream import StreamData


test_configs = [
    {
        "params": {
            "size":       11000,
            "data_width":  8,
            "channels":    1
            },
        "dump_file": "8bit_1ch.txt"
    },
    {
        "params": {
            "size":       11000,
            "data_width": 32,
            "channels":   64
            },
        "dump_file": "32bit_64ch.txt"
    },
]


def prepare_test_configs(tb_image_data_pkg):

    for init_type in ("random", "zero"):
        for test_config in test_configs:

            config_name = \
                "init_type=%s, channels=%s, dumpfile=%s" % \
                (
                    init_type,
                    test_config["params"]["channels"],
                    test_config["dump_file"]
                )

            input_data = StreamData(test_config["params"])

            if init_type == "random":
                input_data.init_with_random(0)
            else:
                input_data.init_with_data(
                    np.zeros(input_data.get_np_shape(), dtype=np.uint32),
                    False,
                    test_config["params"]["data_width"]
                )

            # The default video filter is a simple bypass.
            uut = StreamFilter(
                input_data=input_data,
                dump_filename=test_config["dump_file"]
            )

            generics = uut.vunit_generate_generics()

            generics.update(CHANNELS=test_config["params"]["channels"])

            tb_image_data_pkg.set_sim_option(
                "vhdl_assert_stop_level", "failure")

            tb_image_data_pkg.add_config(
                name=config_name,
                pre_config=uut.vunit_pre_config,
                generics=generics
            )
