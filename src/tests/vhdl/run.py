from vunit import VUnit
import os

import drever
from tests.vhdl import prepare_tb_image_data_pkg
from tests.vhdl import prepare_tb_stream_data_pkg


# Create VUnit instance by parsing command line arguments
vu = VUnit.from_argv()

# Create libraries
drever.add_drever_hdl_sources(vu)
vhdl_tests_dir = os.path.join(drever.__path__[0], "../tests/vhdl/*.vhd")

# Testbenches
test_lib = vu.add_library("test_lib")
test_lib.add_source_files(vhdl_tests_dir)
tb_image_data_pkg = test_lib.entity("tb_image_data_pkg")
tb_stream_data_pkg = test_lib.entity("tb_stream_data_pkg")

prepare_tb_image_data_pkg.prepare_test_configs(tb_image_data_pkg)
prepare_tb_stream_data_pkg.prepare_test_configs(tb_stream_data_pkg)

vu.main()
