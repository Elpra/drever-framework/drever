library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.env.all;

library vunit_lib;
context vunit_lib.vunit_context;

library drever;
use drever.image_data_pkg.all;

entity tb_image_data_pkg is
    generic (
        runner_cfg   : string := runner_cfg_default;
        output_path  : string;
        IN_FILENAME  : string;
        OUT_FILENAME : string;
        USE_BITDEPTH : natural;
        EXP_BITDEPTH : natural;
        CHANNELS     : natural range 1 to 3
    );
end entity tb_image_data_pkg;

architecture RTL of tb_image_data_pkg is

    shared variable ppm_in_vector  : t_image;
    shared variable ppm_out_vector : t_image;

    procedure assert_image_equal_alt(variable a, b : inout t_image) is
        variable a_params : t_params;
        variable a_value  : t_pixel_range;
        variable b_value  : t_pixel_range;
    begin

        -- Image a and b params are expected to be equal
        a_params := ppm_in_vector.get_params;

        for x in 0 to a_params.width-1 loop
            for y in 0 to a_params.height-1 loop
                for channel in 0 to a_params.channels-1 loop

                    if (a_params.channels = 1) then
                        a_value := a.get_entry(x, y);
                        b_value := b.get_entry(x, y);
                    else
                        a_value := a.get_entry(x, y, channel);
                        b_value := b.get_entry(x, y, channel);
                    end if;

                    assert a_value = b_value
                        report "A/B Data Missmatch!"
                        severity error;

                end loop;
            end loop;
        end loop;
    end procedure;

begin

    test_runner : process
        variable in_params  : t_params;
        variable out_params : t_params;
        variable cmp_result : integer;  --@suppress
        variable test_value : t_pixel_range;

        variable assertion_logger : logger_t := get_logger("Assertion Logger");  --@suppress

    begin

        test_runner_setup(runner, runner_cfg);

        ppm_in_vector.init("Input Testvector - Unprocessed Data");
        ppm_out_vector.init("Output Testvector - Processed by Bypass Filter");

        ppm_in_vector.load(output_path & "/" & IN_FILENAME, USE_BITDEPTH);
        ppm_out_vector.load(output_path & "/" & OUT_FILENAME, USE_BITDEPTH);

        in_params  := ppm_in_vector.get_params;
        out_params := ppm_out_vector.get_params;

        assert in_params.channels = CHANNELS
            report "Channels Missmatch!" & LF &
                "Loaded: " & integer'image(in_params.channels) & LF &
                "Expect: " & integer'image(CHANNELS)
            severity failure;

        assert compare_image_params(in_params, out_params)
            report "PPM in/out image parameter missmatch!"
            severity failure;

        assert in_params.bitdepth = EXP_BITDEPTH
            report "Bitdepth determination error " & LF &
                "Is value:   " & integer'image(in_params.bitdepth) & LF &
                "Exp. value: " & integer'image(EXP_BITDEPTH)
            severity failure;

        -- Using alternative assertion procedure for testing get_entry(...) method
        assert_image_equal_alt(ppm_in_vector, ppm_out_vector);

        -- Testing the real image assertion method
        assert_image_equal(ppm_in_vector, ppm_in_vector, "", assertion_logger);

        disable_stop(error);
        info(assertion_logger, "The next two errors are expected to be shown.");

        -- inject a single error pixel
        test_value := ppm_out_vector.get_entry(0, 0, 0) + 1;
        ppm_out_vector.set_entry(test_value, 0, 0, 0);
        assert_image_equal(ppm_in_vector, ppm_out_vector, "", assertion_logger);

        -- inject params missmatch
        ppm_out_vector.set_params(0, 0, 0, 0);
        assert_image_equal(ppm_in_vector, ppm_out_vector, "", assertion_logger);

        -- Two errors are expected
        assert get_log_count(assertion_logger, error) = 2
            severity failure;
        reset_log_count(assertion_logger, error);

        test_runner_cleanup(runner);

    end process;

end architecture RTL;
