library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.env.all;

library vunit_lib;
context vunit_lib.vunit_context;

library drever;
use drever.stream_data_pkg.all;

entity tb_stream_data_pkg is
    generic (
        runner_cfg   : string := runner_cfg_default;
        output_path  : string;
        IN_FILENAME  : string;
        OUT_FILENAME : string;
        CHANNELS     : natural range 0 to 256
    );
end entity tb_stream_data_pkg;

architecture RTL of tb_stream_data_pkg is

    shared variable txt_in_vector  : t_stream;
    shared variable txt_out_vector : t_stream;

    procedure assert_stream_equal_alt(variable a, b : inout t_stream) is
        variable a_params : t_params;
        variable a_value  : t_data_range;
        variable b_value  : t_data_range;
    begin

        -- Image a and b params are expected to be equal
        a_params := txt_in_vector.get_params;

        for x in 0 to a_params.size-1 loop
            for channel in 0 to a_params.channels-1 loop

                if (a_params.channels = 1) then
                    a_value := a.get_entry(x);
                    b_value := b.get_entry(x);
                else
                    a_value := a.get_entry(x, channel);
                    b_value := b.get_entry(x, channel);
                end if;

                assert a_value = b_value
                    report "A/B Data Missmatch!"
                    severity error;

            end loop;
        end loop;
    end procedure;

begin

    test_runner : process
        variable in_params  : t_params;
        variable out_params : t_params;
        variable test_value : t_data_range;

        variable assertion_logger : logger_t := get_logger("Assertion Logger"); --@suppress

    begin

        test_runner_setup(runner, runner_cfg);

        txt_in_vector.init("Input Testvector - Unprocessed Data");
        txt_out_vector.init("Output Testvector - Processed by Bypass Filter");

        txt_in_vector.load(output_path & "/" & IN_FILENAME);
        txt_out_vector.load(output_path & "/" & OUT_FILENAME);

        in_params  := txt_in_vector.get_params;
        out_params := txt_out_vector.get_params;

        assert in_params.channels = CHANNELS
            report "Channels Missmatch!" & LF &
                "Loaded: " & integer'image(in_params.channels) & LF &
                "Expect: " & integer'image(CHANNELS)
            severity failure;

        assert compare_stream_params(in_params, out_params)
            report "TXT in/out image parameter missmatch!"
            severity failure;

        -- Using alternative assertion procedure for testing get_entry(...) method
        assert_stream_equal_alt(txt_in_vector, txt_out_vector);

        -- Testing the real image assertion method
        assert_stream_equal(txt_in_vector, txt_in_vector, "", assertion_logger);

        disable_stop(error);
        info(assertion_logger, "The next two errors are expected to be shown.");

        -- inject a single error pixel
        test_value := txt_out_vector.get_entry(0, 0) + 1;
        txt_out_vector.set_entry(test_value, 0, 0);
        assert_stream_equal(txt_in_vector, txt_out_vector, "", assertion_logger);

        -- inject params missmatch
        txt_out_vector.set_params(0, 0, 0);
        assert_stream_equal(txt_in_vector, txt_out_vector, "", assertion_logger);

        -- Two errors are expected
        assert get_log_count(assertion_logger, error) = 2
            severity failure;
        reset_log_count(assertion_logger, error);

        test_runner_cleanup(runner);

    end process;

end architecture RTL;
