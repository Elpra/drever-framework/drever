'''
Created on Apr 23, 2019

@author: baumannt
'''
import unittest
import sys
import numpy as np

from tests.helpers import capture
from drever.base_class import Drever
from drever.bypass import Bypass


class Test(unittest.TestCase):

    '''
    Base Class Test Class

    The tests are coupled with the Bypass class.
    '''

    def test_base_class(self):
        '''
        Using the Base Class is forbidden and must result in an error.
        '''
        # pylint: disable=E0110
        with self.assertRaises(TypeError):
            _ = Drever()

    def test_bypass_class(self):
        '''
        Tests default run method of the Drever baseclass. The default method
        is a simple bypass procedure, which just returns the input data.
        '''

        test_configs = [
            [(11, 21), np.uint8],
            [(12, 22), np.uint16],
            [(13, 23, 3), np.uint8],
            [(14, 24, 3), np.uint16]
        ]

        for test_config in test_configs:

            info_type = np.iinfo(test_config[1])
            max_val = info_type.max
            rnd_data_float = (max_val*np.random.rand(*test_config[0]))
            rnd_data = rnd_data_float.astype(test_config[1])

            # This tests also the input_data handling of the constructor
            uuts = [
                Bypass(input_data=rnd_data),
                DreverMockNumpyData(input_data=rnd_data)
            ]

            for uut in uuts:

                with self.subTest(uut=uut, rnd_data=rnd_data):
                    uut.run()
                    np.testing.assert_array_equal(
                        uut.get_output_data(),
                        rnd_data,
                        "Data Missmatch!"
                    )

    def test_vector_dump(self):
        '''
        Test vector dumping
        '''

        uut = Bypass()

        # First test without setting vectors
        with self.assertRaises(AssertionError):
            uut.dump_input_vectors("/tmp/Path_to_Vectorfile")

        with self.assertRaises(AssertionError):
            uut.dump_output_vectors("/tmp/Path_to_Vectorfile")

        # Than test with setting vectors
        uut.set_input_data(np.zeros((10, 10)))
        uut.run()

        with self.assertRaises(AssertionError):
            uut.dump_input_vectors("/tmp/Path_to_Vectorfile")

        with self.assertRaises(AssertionError):
            uut.dump_output_vectors("/tmp/Path_to_Vectorfile")

    def test_parameter_handling(self):
        '''
        Test handling of parameters (getter, setter and printing in table form)
        '''

        params = {"A": 123, "B": 456, "C": None}

        uuts = [Bypass(), Bypass(params)]

        # Parameter D will be added to every Bypass() instance, in case that
        # the constructor does not create a new list instead of a pointer.
        uut_a = Bypass(params)
        uut_a.params["D"] = 42

        for uut in uuts:

            with self.subTest("Params dict is no pointer"):
                self.assertNotIn(
                    "D",
                    uut.params,
                    '"D" is not allowed to be a part of the dictionary!'
                )

            with self.subTest("Test parameter getter function get_parameter"):

                for param_key, param_val in params.items():
                    default = 789
                    param_key = uut.get_parameter(param_key, default)

                    if uut.get_params() == {}:
                        self.assertEqual(
                            param_key,
                            default,
                            "get_parameter: Missmatch"
                        )
                    else:
                        self.assertEqual(
                            param_key,
                            param_val,
                            "get_parameter: Missmatch"
                        )

            with self.subTest("Print Parameter Table"):

                # This test is only compatible with Python 3.6 and greater.
                if sys.version_info[1] < 6:  # pragma: no cover
                    uut.print_params_table()
                else:

                    with capture.captured_output() as (out, _):
                        uut.print_params_table()
                    # This can go inside or outside the `with` block
                    output = out.getvalue().strip()

                    if uut.get_params() == {}:

                        expected = \
                            "+-----------------------------------+\n" + \
                            "| No parameters available to print! |\n" + \
                            "+-----------------------------------+"

                    else:

                        expected = \
                            "+----+-------------+----------+\n" + \
                            "|    | Registers   |   Values |\n" + \
                            "|----+-------------+----------|\n" + \
                            "|  0 | A           |      123 |\n" + \
                            "|  1 | B           |      456 |\n" + \
                            "|  2 | C           |      nan |\n" + \
                            "+----+-------------+----------+"

                    self.assertEqual(output, expected)

            with self.subTest("Test parameters getter function get_params"):

                if uut.get_params() == {}:
                    uut.set_params(params)

                self.assertDictEqual(
                    uut.get_params(),
                    params,
                    "get_params: Missmatch"
                )

    def test_vunit_methods(self):
        '''
        Test VUnit related methods
        '''

        uut = Bypass()

        with self.assertRaises(AssertionError):
            uut.set_dump_filename("")

        dump_filename = "vunit_test_dump.tmp"
        uut.set_dump_filename(dump_filename)

        with self.assertRaises(AssertionError):
            uut.vunit_get_dump_filename("Invalid String")

        dump_tests = [
            {"type": "input", "filename": ""},
            {"type": "output", "filename": ""}
        ]

        for dump_test in dump_tests:
            dump_test["filename"] = \
                uut.vunit_get_dump_filename(dump_test["type"])

            self.assertEqual(
                dump_test["filename"],
                dump_test["type"] + "_" + dump_filename,
                "Wrong dump filename"
            )

        self.assertDictEqual(
            uut.vunit_generate_generics(),
            {
                "IN_FILENAME": dump_tests[0]["filename"],
                "OUT_FILENAME": dump_tests[1]["filename"]
            },
            "Dict missmatch!"
        )


class DreverMockNumpyData(Drever):
    '''
    classdocs
    '''

    CLASS_CONFIG = {
        "INPUT_VECTOR_TYPE":  np.ndarray,
        "OUTPUT_VECTOR_TYPE": np.ndarray
    }

    def run(self):
        '''
        The Bypass Drever simply sets the output data as the Input Data
        '''
        self._set_output_data(self.get_input_data())


if __name__ == "__main__":  # pragma: no cover
    unittest.main()
