'''
Created on Jul 09, 2021

@author: baumannt
'''
import unittest
import os
import numpy as np

from tests.helpers import capture
import drever
from drever.data_handlers.stream import StreamData
from drever.data_handlers.stream import DATA_TYPE, DEFAULT_DATA_WIDTH
from drever.data_handlers.stream import compare_stream
from drever.data_handlers.stream import assert_stream_equal


class Test(unittest.TestCase):
    '''
    Testing class for ImageData class
    '''

    tmp_dir = ""

    @classmethod
    def setUpClass(cls):

        # Create temporary directory for testdata files if not jet available
        cls.tmp_dir = os.path.join(drever.__path__[0], "../../tmp")
        if not os.path.exists(cls.tmp_dir):  # pragma: no cover
            os.makedirs(cls.tmp_dir)
            print("tmp directory created: " + cls.tmp_dir)

    def test_set_params(self):

        set_of_valid_params = [
            {"size": 100, "data_width":  8, "channels": 1},
            {"size": 100, "data_width": 10, "channels": 2},
            {"size": 100, "data_width": 12, "channels": 3}
        ]

        uut = StreamData()

        for params in set_of_valid_params:
            with self.subTest(params=params):
                uut.set_params(params)

        set_of_failing_params = [
            {"size": 100,     "data_width":   8, "channels": 257},
            {"size": 2**32+2, "data_width":   8, "channels":   3},
            {"size": 100,     "data_width":  33, "channels":   3},
        ]

        for params in set_of_failing_params:
            with self.subTest(params=params):
                with self.assertRaises(AssertionError):
                    uut.set_params(params)

    def test_extract_params(self):

        set_of_tests = [
            {
                'params': {
                    'dim': (10, 1),
                    'max': 9,
                    'data_width': None
                },
                'expected': {
                    'size': 10,
                    'data_width': DEFAULT_DATA_WIDTH,
                    'channels': 1
                }
            },
            {
                'params': {
                    'dim': (1000, 5),
                    'max': 9,
                    'data_width': "Real"
                },
                'expected': {
                    'size': 1000,
                    'data_width': 4,
                    'channels': 5
                }
            },
            {
                'params': {
                    'dim': (1000, 5),
                    'max': 0,
                    'data_width': "Real"
                },
                'expected': {
                    'size': 1000,
                    'data_width': DEFAULT_DATA_WIDTH,
                    'channels': 5
                }
            },
            {
                'params': {
                    'dim': (1000, ),
                    'max': 90,
                    'data_width': 8
                },
                'expected': {
                    'size': 1000,
                    'data_width': 8,
                    'channels': 1
                }
            },
            {
                'params': {
                    'dim': (1000, ),
                    'max': 0,
                    'data_width': 8
                },
                'expected': {
                    'size': 1000,
                    'data_width': 8,
                    'channels': 1
                }
            },
            {
                'params': {
                    'dim': (1000, ),
                    'max': 0,
                    'data_width': None
                },
                'expected': {
                    'size': 1000,
                    'data_width': DEFAULT_DATA_WIDTH,
                    'channels': 1
                }
            },
        ]

        uut = StreamData()

        for test in set_of_tests:

            data = np.full(
                test['params']['dim'],
                test['params']['max'],
                DATA_TYPE
            )
            extracted = uut.extract_params(data, test['params']['data_width'])

            with self.subTest(extracted=extracted, expected=test['expected']):
                self.assertDictEqual(
                    extracted,
                    test['expected'],
                    'Extracted params missmatch!'
                )

        set_of_failed_tests = [
            {'dim': (10, 1), 'max':   9, 'data_width': 33},
            {'dim': (10, 1), 'max': 256, 'data_width': 8},
        ]

        uut = StreamData()

        for test in set_of_failed_tests:

            data = np.full(
                test['dim'],
                test['max'],
                DATA_TYPE
            )
            with self.assertRaises(AssertionError):
                extracted = uut.extract_params(data, test['data_width'])

    def test_check_params(self):

        set_of_tests = [
            {
                "params": {
                    "size":     100,
                    "data_width": 8,
                    "channels":   3
                },
                "expected": True
            },
            {
                "params": {
                    "size":     100,
                    "data_width": 8
                },
                "expected": False
            },
            {
                "params": {
                    "size":     100,
                    "data_width": 8,
                    "channels":   300
                },
                "expected": False
            }
        ]

        uut = StreamData()

        for test in set_of_tests:
            with self.subTest(test=test):
                self.assertEqual(
                    uut.check_params(test["params"], False),
                    test["expected"],
                    "check_params result missmatch!"
                )

    def test_check_data(self):

        valid_data = np.zeros((100, 3), dtype=DATA_TYPE)

        uut = StreamData()

        with capture.captured_output() as (out, _):
            check_result = uut.check_data(valid_data)

        out_parts = out.getvalue().strip().split('\n')

        self.assertIn("'size': 100", out_parts[4])
        self.assertIn("'data_width': 16", out_parts[4])
        self.assertIn("'channels': 3", out_parts[4])

        del out_parts[4]

        self.assertEqual(
            "\n".join(out_parts),
            "*************************************************\n" +
            "Data Check Error\n" +
            "*************************************************\n" +
            "- Extracted Parameters:\n"
            "- Set Parameters:\n" +
            "None\n" +
            "*************************************************"
        )

        self.assertFalse(
            check_result,
            "check_data should return False because no params were set!"
        )

        uut.set_params({
            "size": 100,
            "data_width": 8,
            "channels": 3
        })

        self.assertTrue(
            uut.check_data(valid_data, 8),
            "check_data should return True because params were set!"
        )

    @classmethod
    def test_init_with_data(cls):
        init_data = np.zeros((10, 3), DATA_TYPE)
        uut = StreamData()
        uut.init_with_data(init_data, True, 8)
        uut_data = uut.get_stream_data()
        np.testing.assert_array_equal(uut_data, init_data)

    def test_init_with_random(self):

        uut = StreamData()

        uut.set_params({
            "size":       5000,
            "data_width":    8,
            "channels":      3
        })

        uut.init_with_random(seed=42)
        self.assertTrue(np.all(uut.get_stream_data() >= 0), "Lower Limit Missmatch")
        self.assertTrue(np.all(uut.get_stream_data() < 2**8),  "Upper Limit Missmatch")

        limits = ((0, 1), (100, 255))

        for limit in limits:
            uut.init_with_random(42, limit[0], limit[1])
            self.assertTrue(np.all(uut.get_stream_data() >= limit[0]), "Lower Limit Missmatch")
            self.assertTrue(np.all(uut.get_stream_data() <= limit[1]), "Upper Limit Missmatch")

        limits = ((0, 300),)

        for limit in limits:
            with self.assertRaises(AssertionError):
                uut.init_with_random(42, limit[0], limit[1])

    def test_equal_method(self):

        size = 100

        set_of_valid_params = [
            {"size": size, "data_width":  8, "channels": 1},
            {"size": size, "data_width": 10, "channels": 2},
            {"size": size, "data_width": 12, "channels": 3}
        ]

        # Let's start with working tests
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut_a = StreamData(params)
                uut_a.init_with_random(0)

                uut_b = StreamData(params)
                uut_b.init_with_random(0)

                self.assertEqual(
                    uut_a,
                    uut_b,
                    "Data missmatch!"
                )

        # Equality not given due missmatching stream data
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut_a = StreamData(params)
                uut_a.init_with_random(0)

                uut_b = StreamData(params)
                uut_b.init_with_random(1)

                self.assertNotEqual(
                    uut_a,
                    uut_b,
                    "Data missmatch!"
                )

        # Equality not given due missmatching image params
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut_a = StreamData(params)
                uut_a.init_with_random(0)

                uut_b = StreamData(params.copy())
                uut_b.init_with_random(0)
                uut_b.params['data_width'] = 16

                self.assertNotEqual(
                    uut_a,
                    uut_b,
                    "Params missmatch!"
                )

        # Equality not given due missmatching class type
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut_a = StreamData(params)
                uut_a.init_with_random(0)

                uut_b = StreamData(params)
                uut_b.init_with_random(0)

                # unsupported types must return NotImplemented flag
                self.assertEqual(
                    uut_a.__eq__("Strings aren't objs from ImageData class."),
                    NotImplemented,
                    "Type missmatch!"
                )

                self.assertEqual(
                    uut_a,
                    uut_b.get_stream_data(),
                    "Type missmatch!"
                )

    def test_set_get_item(self):

        size = 100

        test_value = 42

        set_of_valid_params = [
            {"size": size, "data_width":  8, "channels": 1},
            {"size": size, "data_width":  8, "channels": 3}
        ]

        # Let's start with working tests
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut = StreamData(params)
                uut.init_with_random(0)

                for i in range(size):
                    uut[i, 0] = test_value
                    self.assertEqual(uut[i, 0], test_value, "Data missmatch!")

    def test_save_load(self):

        def add_empty_line(filename):
            with open(filename, 'r') as tmp_file:
                origin_content = tmp_file.read()
                tmp_file.close()
            with open(filename, 'w') as tmp_file:
                tmp_file.write("    \n")
                tmp_file.write(origin_content)
                tmp_file.close()

        txt_files = [
            {
                "params": {
                    "size":       10,
                    "data_width":  8,
                    "channels":    1
                },
                "path": "sd_8bit_1ch.txt"
            },
            {
                "params": {
                    "size":       20,
                    "data_width":  8,
                    "channels":    8
                },
                "path": "sd_8bit_8ch.txt"
            },
        ]

        for txt_file in txt_files:

            for init_type in ("random", "zero"):

                uut_a = StreamData()
                uut_a.set_params(txt_file["params"])

                if init_type == "random":
                    uut_a.init_with_random(0)
                else:
                    uut_a.init_with_data(
                        np.zeros(uut_a.get_np_shape(), dtype=DATA_TYPE),
                        False,
                        txt_file["params"]["data_width"]
                    )

                filename = os.path.join(self.tmp_dir, init_type + "_" + txt_file["path"])

                with self.subTest(params=txt_file["path"]):
                    uut_a.save(filename, True)

                    # Saving again must raise an error
                    with self.assertRaises(FileExistsError):
                        uut_a.save(filename)

                    uut_b = StreamData()

                    # Loading non existing file must raise an error
                    with self.assertRaises(FileNotFoundError):
                        uut_b.load(filename + ".tmp")

                    uut_b.load(filename)

                    np.testing.assert_array_equal(
                        uut_a.get_stream_data(),
                        uut_b.get_stream_data(),
                        "Data Missmatch load with FreeImage"
                    )

                    # Add empty lines to see if bypassing is working
                    add_empty_line(filename)

                    uut_b.load(filename)

                    np.testing.assert_array_equal(
                        uut_b.get_stream_data(),
                        uut_a.get_stream_data(),
                        "Data Missmatch"
                    )

    def test_stream_assertions(self):

        stream_a = StreamData()
        stream_b = StreamData()

        stream_a.set_params({
            "size":       10,
            "data_width": 10,
            "channels":    8
        })
        stream_a.init_with_random(0)

        stream_b.set_params(stream_a.params)
        stream_b.init_with_random(0)

        # Check equal
        result = compare_stream(stream_a, stream_b)
        self.assertEqual(result['num_errors'], 0, "Number of errors missmatch")
        assert_stream_equal(stream_a, stream_b)

        # Check none equal by injecting two errors
        stream_b.stream_data[2, 3] += 1
        stream_b.stream_data[5, 6] += 1
        result = compare_stream(stream_a, stream_b)
        self.assertEqual(result['num_errors'], 2, "Number of errors missmatch")
        self.assertEqual(
            result['first_error'],
            (2, 3),
            "Coordinates of first errors missmatch"
        )
        with self.assertRaises(AssertionError):
            assert_stream_equal(stream_a, stream_b)

        # Check non equal parameters
        stream_b.set_params({
            "size":       10,
            "data_width": 10,
            "channels":    3
        })
        result = compare_stream(stream_a, stream_b)
        self.assertEqual(result['num_errors'], -1, "Parameters check failed")
        with self.assertRaises(AssertionError):
            assert_stream_equal(stream_a, stream_b)


if __name__ == "__main__":  # pragma: no cover
    unittest.main()
