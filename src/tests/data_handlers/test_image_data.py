'''
Created on Apr 18, 2019

@author: baumannt
'''
import unittest
import os
import numpy as np
import imageio

from tests.helpers import capture
import drever
from drever.data_handlers.image import ImageData, DEFAULT_BITDEPTH
from drever.data_handlers.image import compare_image, assert_image_equal


class Test(unittest.TestCase):
    '''
    Testing class for ImageData class
    '''

    tmp_dir = ""

    @classmethod
    def setUpClass(cls):

        # Create temporary directory for testdata files if not jet available
        cls.tmp_dir = os.path.join(drever.__path__[0], "../../tmp")
        if not os.path.exists(cls.tmp_dir):  # pragma: no cover
            os.makedirs(cls.tmp_dir)
            print("tmp directory created: " + cls.tmp_dir)

    def test_set_params(self):

        set_of_valid_params = [
            {"width": 100, "height": 100, "bitdepth":  8, "channels": 1},
            {"width": 100, "height": 100, "bitdepth": 10, "channels": 2},
            {"width": 100, "height": 100, "bitdepth": 12, "channels": 3}
        ]

        uut = ImageData()

        for params in set_of_valid_params:
            with self.subTest(params=params):
                uut.set_params(params)

        set_of_failing_params = [
            {"width": 100, "height":   100, "bitdepth":  8},
            {"width":   0, "height":   100, "bitdepth": 10, "channels": 2},
            {"width": 100, "height": 10000, "bitdepth": 12, "channels": 3},
            {"width": 100, "height":   100, "bitdepth": 17, "channels": 3},
            {"width": 100, "height":   100, "bitdepth": 12, "channels": 4}
        ]

        for params in set_of_failing_params:
            with self.subTest(params=params):
                with self.assertRaises(AssertionError):
                    uut.set_params(params)

    def test_extract_params(self):

        set_of_tests = [
            {
                'params': {
                    'dim': (10, 20),
                    'max': 9,
                    'bitdepth': None
                },
                'expected': {
                    'width':   20,
                    'height':  10,
                    'bitdepth': 8,
                    'channels': 1
                }
            },
            {
                'params': {
                    'dim': (10, 20),
                    'max': 65535,
                    'bitdepth': None
                },
                'expected': {
                    'width':    20,
                    'height':   10,
                    'bitdepth': 16,
                    'channels':  1
                }
            },
            {
                'params': {
                    'dim': (11, 2001, 1),
                    'max': 4000,
                    'bitdepth': "Real"
                },
                'expected': {
                    'width':  2001,
                    'height':   11,
                    'bitdepth': 12,
                    'channels':  1
                }
            },
            {
                'params': {
                    'dim': (11, 2001, 1),
                    'max': 0,
                    'bitdepth': "Real"
                },
                'expected': {
                    'width':  2001,
                    'height':   11,
                    'bitdepth': DEFAULT_BITDEPTH,
                    'channels':  1
                }
            },
            {
                'params': {
                    'dim': (300, 400, 3),
                    'max': 160,
                    'bitdepth': 8
                },
                'expected': {
                    'width':  400,
                    'height': 300,
                    'bitdepth': 8,
                    'channels': 3
                }
            },
        ]

        uut = ImageData()

        for test in set_of_tests:

            data = np.full(
                test['params']['dim'],
                test['params']['max'],
                np.uint16
            )
            extracted = uut.extract_params(data, test['params']['bitdepth'])

            with self.subTest(extracted=extracted, expected=test['expected']):
                self.assertDictEqual(
                    extracted,
                    test['expected'],
                    'Extracted params missmatch!'
                )

    def test_check_data(self):

        valid_data = np.zeros((100, 200, 3), dtype=np.uint16)

        uut = ImageData()

        with capture.captured_output() as (out, _):
            check_result = uut.check_data(valid_data)

        out_parts = out.getvalue().strip().split('\n')

        self.assertIn("'width': 200", out_parts[4])
        self.assertIn("'height': 100", out_parts[4])
        self.assertIn("'bitdepth': 8", out_parts[4])
        self.assertIn("'channels': 3", out_parts[4])

        del out_parts[4]

        self.assertEqual(
            "\n".join(out_parts),
            "*************************************************\n" +
            "Data Check Error\n" +
            "*************************************************\n" +
            "- Extracted Parameters:\n"
            "- Set Parameters:\n" +
            "None\n" +
            "*************************************************"
        )

        self.assertFalse(
            check_result,
            "check_data should return False because no params were set!"
        )

        uut.set_params({
            "width":  200,
            "height": 100,
            "bitdepth": 8,
            "channels": 3
        })

        self.assertTrue(
            uut.check_data(valid_data, 8),
            "check_data should return True because params were set!"
        )

    def test_check_params(self):

        set_of_tests = [
            {
                "params": {
                    "width":  200,
                    "height": 100,
                    "bitdepth": 8,
                    "channels": 3
                },
                "expected": True
            },
            {
                "params": {
                    "width":  200,
                    "height": 100,
                    "bitdepth": 8
                },
                "expected": False
            },
            {
                "params": {
                    "width":  200,
                    "height": 100,
                    "bitdepth": 8,
                    "channels": 4
                },
                "expected": False
            }
        ]

        uut = ImageData()

        for test in set_of_tests:
            with self.subTest(test=test):
                self.assertEqual(
                    uut.check_params(test["params"], False),
                    test["expected"],
                    "check_params result missmatch!"
                )

    @classmethod
    def test_init_with_data(cls):

        init_data = np.zeros((10, 20, 3), np.uint16)

        uut = ImageData()
        uut.init_with_data(init_data, True, 8)

        np.testing.assert_array_equal(uut.get_image_data(), init_data)

    @classmethod
    def test_init_with_random(cls):

        uut = ImageData()

        uut.set_params({
            "width":    2,
            "height":   1,
            "bitdepth": 8,
            "channels": 3
        })

        uut.init_with_random(seed=42)
        uut.init_with_random()

    def test_equal_method(self):

        width = 100
        height = 100

        set_of_valid_params = [
            {"width": width, "height": height, "bitdepth":  8, "channels": 1},
            {"width": width, "height": height, "bitdepth": 10, "channels": 2},
            {"width": width, "height": height, "bitdepth": 12, "channels": 3}
        ]

        # Let's start with working tests
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut_a = ImageData(params)
                uut_a.init_with_random(0)

                uut_b = ImageData(params)
                uut_b.init_with_random(0)

                self.assertEqual(
                    uut_a,
                    uut_b,
                    "Data missmatch!"
                )

        # Equality not given due missmatching image data
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut_a = ImageData(params)
                uut_a.init_with_random(0)

                uut_b = ImageData(params)
                uut_b.init_with_random(1)

                self.assertNotEqual(
                    uut_a,
                    uut_b,
                    "Data missmatch!"
                )

        # Equality not given due missmatching image params
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut_a = ImageData(params)
                uut_a.init_with_random(0)

                uut_b = ImageData(params.copy())
                uut_b.init_with_random(0)
                uut_b.params['bitdepth'] = 16

                self.assertNotEqual(
                    uut_a,
                    uut_b,
                    "Params missmatch!"
                )

        # Equality not given due missmatching class type
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut_a = ImageData(params)
                uut_a.init_with_random(0)

                uut_b = ImageData(params)
                uut_b.init_with_random(0)

                # unsupported types must return NotImplemented flag
                self.assertEqual(
                    uut_a.__eq__("Strings aren't objs from ImageData class."),
                    NotImplemented,
                    "Type missmatch!"
                )

                self.assertEqual(
                    uut_a,
                    uut_b.get_image_data(),
                    "Type missmatch!"
                )

    def test_set_get_item(self):

        width = 100
        height = 100

        test_value = 42

        set_of_valid_params = [
            {"width": width, "height": height, "bitdepth":  8, "channels": 1},
            {"width": width, "height": height, "bitdepth":  8, "channels": 3}
        ]

        # Let's start with working tests
        for params in set_of_valid_params:
            with self.subTest(params=params):

                uut = ImageData(params)
                uut.init_with_random(0)

                x_coord = int(width/2)
                y_coord = int(height/2)

                uut[x_coord, y_coord, 0] = test_value

                self.assertEqual(
                    uut[x_coord, y_coord, 0],
                    test_value,
                    "Data missmatch!"
                )

    def test_save_load(self):

        def add_empty_line(filename):
            with open(filename, 'r') as tmp_file:
                origin_content = tmp_file.read()
                tmp_file.close()
            with open(filename, 'w') as tmp_file:
                tmp_file.write("    \n")
                tmp_file.write(origin_content)
                tmp_file.close()

        ppm_files = [
            {
                "params": {
                    "width":    20,
                    "height":   10,
                    "bitdepth":  8,
                    "channels":  1
                },
                "path": "id_8bit_gray.ppm"
            },
            {
                "params": {
                    "width":    20,
                    "height":   10,
                    "bitdepth": 10,
                    "channels":  2
                },
                "path": "id_10bit_2chans.ppm"
            },
            {
                "params": {
                    "width":    20,
                    "height":   10,
                    "bitdepth": 16,
                    "channels":  3
                },
                "path": "id_16bit_rgb.ppm"
            }
        ]

        for ppm_file in ppm_files:

            for init_type in ("random", "zero"):

                uut_a = ImageData()
                uut_a.set_params(ppm_file["params"])

                if init_type == "random":
                    uut_a.init_with_random(0)
                else:
                    uut_a.init_with_data(
                        np.zeros(uut_a.get_np_shape(), dtype=np.uint16),
                        False,
                        ppm_file["params"]["bitdepth"]
                    )

                filename = os.path.join(
                    self.tmp_dir, init_type + "_" + ppm_file["path"]
                )

                # Prepare file with ImageIO and FreeImage
                fi_filename = os.path.join(
                    self.tmp_dir,
                    "fi_" + init_type + "_" + ppm_file["path"]
                )

                if ppm_file["params"]["channels"] == 1:
                    fi_data = uut_a.get_image_data()[::, ::, 0]
                elif ppm_file["params"]["channels"] == 2:
                    fi_data = np.insert(uut_a.get_image_data(), 2, 0, axis=2)
                else:
                    fi_data = uut_a.get_image_data()

                imageio.imwrite(fi_filename, fi_data, "PPM-FI", flags=1)

                with self.subTest(params=ppm_file["path"]):
                    uut_a.save(filename, True)

                    # Saving again must raise an error
                    with self.assertRaises(FileExistsError):
                        uut_a.save(filename)

                    uut_b = ImageData()
                    uut_a_fi = ImageData()

                    # Loading non existing file must raise an error
                    with self.assertRaises(FileNotFoundError):
                        uut_b.load(filename + ".tmp")

                    uut_a_data = imageio.imread(filename, "PPM-FI")

                    if ppm_file["params"]["bitdepth"] in range(9, 16):
                        uut_a_data = (uut_a_data / 64).astype(np.uint16)

                    np.testing.assert_array_equal(
                        uut_a_data,
                        fi_data,
                        "Data Missmatch load with FreeImage"
                    )

                    # Add empty lines to see if bypassing is working
                    add_empty_line(filename)

                    # When using two channels, they have to be set explicitly
                    # after loading!
                    if ppm_file["params"]["channels"] == 2:
                        uut_b.load(filename, True)
                        uut_a_fi.load(fi_filename, True)
                    else:
                        uut_b.load(filename)
                        uut_a_fi.load(fi_filename)

                    np.testing.assert_array_equal(
                        uut_b.get_image_data(),
                        uut_a.get_image_data(),
                        "Data Missmatch"
                    )

                    np.testing.assert_array_equal(
                        uut_a_fi.get_image_data(),
                        uut_a.get_image_data(),
                        "Data Missmatch load from FreeImage"
                    )

    def test_image_assertions(self):

        im_a = ImageData()
        im_b = ImageData()

        im_a.set_params({
            "width":    20,
            "height":   10,
            "bitdepth": 10,
            "channels":  3
        })
        im_a.init_with_random(0)

        im_b.set_params(im_a.params)
        im_b.init_with_random(0)

        # Check equal
        result = compare_image(im_a, im_b)
        self.assertEqual(result['num_errors'], 0, "Number of errors missmatch")
        assert_image_equal(im_a, im_b)

        # Check none equal by injecting two errors
        im_b.image_data[0, 0, 0] += 1
        im_b.image_data[0, 0, 1] += 1
        result = compare_image(im_a, im_b)
        self.assertEqual(result['num_errors'], 2, "Number of errors missmatch")
        self.assertEqual(
            result['first_error'],
            (0, 0, 0),
            "Coordinates of first errors missmatch"
        )
        with self.assertRaises(AssertionError):
            assert_image_equal(im_a, im_b)

        # Check non equal parameters
        im_b.set_params({
            "width":    21,
            "height":   10,
            "bitdepth": 10,
            "channels":  3
        })
        result = compare_image(im_a, im_b)
        self.assertEqual(result['num_errors'], -1, "Parameters check failed")
        with self.assertRaises(AssertionError):
            assert_image_equal(im_a, im_b)


if __name__ == "__main__":  # pragma: no cover
    unittest.main()
