'''
Created on May 6, 2019

@author: baumannt
'''
import unittest
import os
import numpy as np

import drever
from drever.stream_filter import StreamFilter
from drever.data_handlers.stream import StreamData


class Test(unittest.TestCase):

    tmp_dir = ""

    @classmethod
    def setUpClass(cls):

        # Create temporary directory for testdata files if not yet available
        cls.tmp_dir = os.path.join(drever.__path__[0], "../../tmp")
        if not os.path.exists(cls.tmp_dir):  # pragma: no cover
            os.makedirs(cls.tmp_dir)
            print("tmp directory created: " + cls.tmp_dir)

    def test_default_video_filter(self):

        set_of_valid_params = [
            {"size": 100, "data_width":  8, "channels": 1},
            {"size": 100, "data_width": 16, "channels": 8},
            {"size": 100, "data_width": 32, "channels": 16}
        ]

        for params in set_of_valid_params:
            with self.subTest(params=params):
                uut = StreamFilter()

                stimulus = StreamData(params)
                stimulus.init_with_random()

                uut.set_input_data(stimulus)
                uut.run()

                self.assertEqual(
                    uut.get_output_data(),
                    stimulus,
                    "Data missmatch!"
                )

    def test_vector_dump(self):

        txt_files = [
            {
                "params": {
                    "size":       10,
                    "data_width":  8,
                    "channels":    1
                },
                "path": "sf_8bit_gray.txt"
            },
            {
                "params": {
                    "size":       10,
                    "data_width": 16,
                    "channels":    2
                },
                "path": "sf_16bit_2chans.txt"
            },
            {
                "params": {
                    "size":       10,
                    "data_width": 32,
                    "channels":    3
                },
                "path": "sf_32bit_rgb.txt"
            }
        ]

        uut = StreamFilter()

        for txt_file in txt_files:
            with self.subTest(params=txt_file["params"]):
                uut_a = StreamData()
                uut_a.set_params(txt_file["params"])
                uut_a.init_with_random(0)
                filename = os.path.join(self.tmp_dir, txt_file["path"])

                # Cleanup if files exists from previous runs
                if os.path.exists(filename):  # pragma: no cover
                    os.remove(filename)

                uut.set_input_data(uut_a)
                uut.dump_input_vectors(filename)

                # Saving again must raise an error
                with self.assertRaises(FileExistsError):
                    uut.dump_input_vectors(filename)

                uut_b = StreamData()

                # Loading non existing file must raise an error
                with self.assertRaises(FileNotFoundError):
                    uut_b.load(filename + ".tmp")

                # Add empty lines to see if bypassing is working
                with open(filename, 'r') as uut_a_file:
                    origin_content = uut_a_file.read()
                    uut_a_file.close()
                with open(filename, 'w') as uut_a_file:
                    uut_a_file.write("    \n")
                    uut_a_file.write(origin_content)
                    uut_a_file.close()

                uut_b.load(filename)

                np.testing.assert_array_equal(
                    uut_b.get_stream_data(),
                    uut_a.get_stream_data(),
                    "Data Missmatch"
                )


if __name__ == "__main__":  # pragma: no cover
    unittest.main()
