# Drever Framework

Drever is a framework for supporting the development of FPGA based IPs using Python as specification entry.

## Documentation (under development)

Auto-generated documentation for the latest version of the master branch can be found at [https://elpra.gitlab.io/drever-framework/drever](https://elpra.gitlab.io/drever-framework/drever).

## Requirements

- [Python](https://www.python.org/) 3.5 or above (recommended 3.7)
- [Numpy](https://www.numpy.org/)
- [VUnit](https://vunit.github.io/)

## Development Status

Drever is currently in `alpha` status, which means that newer Drever versions can break compability. It is recommended to tag freeze the Drever version within your development cycle until you have ensured that Drever updates do not have an unwanted impact on your existing code.

All alpha versions are tagged as 0.0.x, while x is counted up straight forward. When entering beta phase, version numbers will be 0.x.0. With the first stable release, the version number becames 1.0.0 and then follows semantic versioning.

## Test Reports

Some test reports are generated for quality ensurance. These can be found at:

- [Functional Verification / Unit Tests](https://elpra.gitlab.io/drever-framework/drever/reports/tests)
- [Code Coverage](https://elpra.gitlab.io/drever-framework/drever/reports/coverage)
- [Code Linting](https://elpra.gitlab.io/drever-framework/drever/reports/lint)

## Licensing

Drever is released under the terms of [GNU General Public License, Version 3](LICENSE).

Copyright (c) 2019, Tobias Baumann tobias.baumann@elpra.de

If you need support for the Drever framework under other license conditions, please get in contact by using the email mentioned above.
