## Release Preparation:
- [ ] Release branch & MR created
- [ ] All issue MRs are picked into release branch
    - !tbd
    - ...

## Post-Deployment
- [ ] New tag with version number created (tag format x.y.z)
- [ ] Successfully deployed to [PyPI server](https://pypi.org/project/drever)

/label ~release
