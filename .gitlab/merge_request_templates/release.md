## What does this MR do?

Prepares release

## What are the relevant issue numbers?

Closes #xx.

## Does this MR meet the acceptance criteria?

### Conformity

- [ ] All planed modifications are picked into this Release-Branch.
- [ ] Conform by Style Guides / Coding Practices
- [ ] Version Number updated in [pkg_info.py](src/drever/pkg_info.py)
- [ ] [CHANGELOG.md](CHANGELOG.md) File is updated and complete

### Verification

- [ ] Functional coverage / Code Coverage has same level than the release before
