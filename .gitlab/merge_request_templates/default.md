## What does this MR do?
Specify the content of the merge request.

## Are there points in the code the reviewer needs to double check?
Point to critical paths where reviewer should have a closer look.

## Why was this MR needed?
Explain the reason of the merge request.

## What are the relevant issue numbers?
Specify the issue numbers on which the merge request belongs. (Use "Closes #xx" for automatic issue closing on accepting.)

## Does this MR meet the acceptance criteria?

### Conformity

- [ ] Changelog Entry
- [ ] Documentation created/updated or Follow-Up Review Issue created
- [ ] Conform by Style Guides / Coding Practices

### Verification

- [ ] Unit Tests written / updated
