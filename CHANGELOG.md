# Changelog

## 0.0.9

### Changed

- Data Handler - `StreamData`: Use `uint32` as data type (see #31).


## 0.0.8

### Fixed

- Fix `init_with_random` limit handling in `StreamData` (see #29).

## 0.0.7

### Added

- Data Handler - `StreamData`: Class for handling simple streaming data (see #26).

### Fixed

- Add Python 3.9 support (see #27).

## 0.0.6

### Fixed

- Optimize `add_drever_hdl_sources()` method as workaround for VUnit bug (see #23)

### Added

- Add `print_params_table()` method to Base Class (see #17).

### Fixed

- Fix initial parameter creation in base class constructor (see #18).
- Fix `extract_params()` method in `ImageData` class (see #19).

## 0.0.5

### Added

- Assertion methods to compare image data (see #12).
- Add Drever base class parameter getter method (see #14).
- Replace venv with Pipenv (see #13).

### Fixed

- Fix handling of black image data testvector files (see #11).

## 0.0.4

### Fixed

- Add package requirements which are automatically fulfilled when running `pip install`.

## 0.0.3

### Fixed

- VHDL sources weren't add to the PyPI package.

## 0.0.2

### Added

- VideoFilter Class: A skeleton class for the development of video/image filters.
- Data Handler - ImageData: ImageData class is a data handler mainly used in the VideoFilter class.

## 0.0.1

Initial Release
